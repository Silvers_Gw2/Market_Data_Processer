{
  description = "DataWars2 API";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-22.05";
    utils.url = "github:numtide/flake-utils";
  };

  nixConfig.bash-prompt-suffix = "[Dw2 Dev] ";

  outputs = { self, nixpkgs, utils, ... }: utils.lib.eachDefaultSystem (system:
    let

      pkgs = import nixpkgs {
        inherit system;
        overlays = [ ];
      };

      package_name = "api_dw2";
      # name in teh package.json
      package_name_alt = "gw2_api_market_data";

      # set the node version here
      nodejs = pkgs.pkgs.nodejs-16_x;

    in rec {

      devShell = pkgs.mkShell {
        name = "Dw2 build env";
        nativeBuildInputs = [
          nodejs
        ];
        shellHook = ''export EDITOR="${pkgs.nano}/bin/nano --nonewlines"'';
      };

      packages."${package_name}" = let
          original = pkgs.mkYarnPackage {
            name = "${package_name}";
            buildInputs = [
              nodejs
            ];
            src = ./.;
            packageJSON = "${./package.json}";
            yarnLock = "${./yarn.lock}";
            buildPhase = "yarn build";
          };
        # takes teh outpout and makes it nicer
        in pkgs.stdenv.mkDerivation {
          name = "${package_name}";
          src = original;
          installPhase = ''
            mkdir -p $out
            cp -R $src/libexec/${package_name_alt}/deps/${package_name_alt}/. $out
            rm $out/node_modules
            cp -R $src/libexec/${package_name_alt}/node_modules/. $out/node_modules
          '';
        };


      defaultPackage = packages."${package_name}";

      nixosModule = { lib, pkgs, config, ... }:
        with lib;
        let
          cfg = config.services."${package_name}";
          
          service_name = script: lib.strings.sanitizeDerivationName("${cfg.prefix}${cfg.user}@${script}");
          
          root_dir = self.defaultPackage."${system}";
          
          # oneshot scripts to run
          serviceGenerator = entry: {
            "${service_name entry.script}" = {
              description = "Dw2 API ${entry.script}";
              wantedBy = [];
              after = [];
              serviceConfig = {
                Type = "oneshot";
                DynamicUser=true;
                ExecStart = "${nodejs}/bin/node ${root_dir}${scripts_path}${entry.script}";
                WorkingDirectory="${root_dir}";
                EnvironmentFile = "${cfg.config}";
              } // lib.optionalAttrs (!(isNull entry.timeout)) {TimeoutStartSec="${entry.timeout}";};
            };
          };
          
          # each timer will run the above service
          timerGenerator = entry: {
            "${service_name entry.script}" = {
              description="Run Dw2 API ${entry.script}";
              wantedBy = ["timers.target"];
              partOf = ["${service_name entry.script}.service"];
              timerConfig = {
                OnCalendar = entry.timer;
                Unit = "${service_name entry.script}.service";
                Persistent=true;
              };
            };
          };
          
          # modify these
          scripts_path = "/build/src/JS/";
          scripts = [
            # minute
            {script = "controller_build.js"; timer="*-*-* *:*:00"; timeout=null;}
            {script = "controller_listings.js"; timer="*:0/4"; timeout="180";}
            {script = "controller_gems.js"; timer="*:0/15"; timeout=null;}

            # hour
            {script = "controller_general.js"; timer="*:00:00"; timeout=null;}
            {script = "controller_itemData.js"; timer="*:00:00"; timeout=null;}
            {script = "controller_misc.js"; timer="*:00:00"; timeout=null;}
            {script = "controller_gemstore.js"; timer="*:00:00"; timeout=null;}

            # controller_listingsProcessing
            {script = "controller_listingsProcessing.js min"; timer="*:00/15"; timeout=null;}
            {script = "controller_listingsProcessing.js hour"; timer="00/4:00"; timeout=null;}
            {script = "controller_listingsProcessing.js day"; timer="00:30:00"; timeout=null;}

          ];
          
        in {
          options.services."${package_name}" = {
            enable = mkEnableOption "enable ${package_name}";

            config = mkOption rec {
              type = types.str;
              default = "./.env";
              example = default;
              description = "The env file";
            };

            prefix = mkOption rec {
              type = types.str;
              default = "silver_";
              example = default;
              description = "The prefix used to name service/folders";
            };

            user = mkOption rec {
              type = types.str;
              default = "${package_name}";
              example = default;
              description = "The user to run the service";
            };

          };

          config = mkIf cfg.enable {

            systemd.services = lib.mkMerge (
              [
                {
                  "${cfg.prefix}${cfg.user}" = {
                    description = "Dw2 API";

                    wantedBy = [ "multi-user.target" ];
                    after = [ ];
                    wants = [ ];
                    serviceConfig = {
                      # fill figure this out in teh future
                      DynamicUser=true;
                      Restart = "always";
                      ExecStart = "${nodejs}/bin/node ${root_dir}";
                      WorkingDirectory="${root_dir}";
                      EnvironmentFile = "${cfg.config}";
                    };
                  };

                # add the other services
                }
              ]
              ++ (lib.lists.forEach scripts serviceGenerator)
            );
             
            # timers to run the above services
            systemd.timers = lib.mkMerge (lib.lists.forEach scripts timerGenerator);
   
          };

        };

    });
}