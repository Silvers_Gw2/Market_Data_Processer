import { name,version, port, env, db } from "./config/config"
import restify from "restify"
import { DB_Manager } from "./config/db"
import { db_entities } from './config/db_entities'
import { routes } from "./config/routeController"
import corsMiddleware from "restify-cors-middleware";

export interface Ctx {
  database: DB_Manager
  server:  restify.Server
}

// setup server
const server = restify.createServer({ name: name, version: version })

// CORS
const cors = corsMiddleware({origins: ["*"], allowHeaders: ["*"], exposeHeaders: ["*"]})
server.pre(cors.preflight);
server.use(cors.actual);

server.use(restify.plugins.bodyParser({ mapParams: true }))
server.use(restify.plugins.acceptParser(server.acceptable))
server.use(restify.plugins.queryParser({ mapParams: true }))
server.use(restify.plugins.fullResponse())



server.listen(port, async () => {
  let database = new DB_Manager()
  await database.lift(db.uri, db.database, db_entities)

  routes({ database, server })

  // report success
  console.log(`[info]: ${name} ${version} ready to accept connections on port ${port} in ${env} environment.`)
})
