import {Ctx} from "../Gw2_API";
import {beautifyJSON} from '../JS/functions';

export const Status = (ctx: Ctx) =>  {
  const {server} = ctx;

  server.get('/gw2/v1/status', async (req, res, next) => {
    let beautify = req.query.beautify || 'human'
    res.sendRaw(200, beautifyJSON({ success: true }, beautify), { 'Content-Type': 'application/json; charset=utf-8' })

    next()
  })
}
