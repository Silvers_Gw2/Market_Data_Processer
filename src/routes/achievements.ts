import {Ctx} from "../Gw2_API";
import {defaultPath} from '../JS/functions';

import {Achievements as Achievements_Type} from "../config/db_entities"

export const Achievements = (ctx: Ctx) =>  {
  const {database,server} = ctx;

  let active = { beautify: true, fields: true, project: true, filter: true, ids: true, itemID: false, start: false, end: false }
  let defaultFields = 'id,icon,name,description,requirement,locked_text,type,flags,tiers,prerequisites,rewards,bits,point_cap,firstAdded'

  server.get('/gw2/v1/achievements/json', async (req, res, next) => {
    await defaultPath(database, req, res, Achievements_Type, req.url, active, defaultFields)
    next()
  })

  server.get('/gw2/v1/achievements/keys', async (req, res, next) => {
    await defaultPath(database, req, res, Achievements_Type, req.url, active, defaultFields, 'key')
    next()
  })
}
