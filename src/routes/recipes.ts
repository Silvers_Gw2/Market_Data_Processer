import {Ctx} from "../Gw2_API";
import {defaultPath} from '../JS/functions';
import {Recipes as Recipes_Type} from "../config/db_entities"

export const Recipes = (ctx: Ctx) =>  {
  const {database,server} = ctx;

  let active = { beautify: true, fields: true, project: true, filter: true, ids: true, itemID: false, start: false, end: false }
  let defaultFind = {}

  // This remains, but wont be updated
  server.get('/gw2/v1/recipes/csv', async (req, res, next) => {
    let collection = 'recipeInfo'
    let defaultFields = 'id,createdBy,output,outputQuantity,item_1_id,item_1_quantity,item_2_id,item_2_quantity,item_3_id,item_3_quantity,item_4_id,item_4_quantity,lastUpdate'

    await defaultPath(database, req, res, collection, req.url, active, defaultFields, 'csv', defaultFind)
    next()
  })

  server.get('/gw2/v1/recipes/json', async (req, res, next) => {
    let collection = 'recipeInfo'
    let defaultFields = 'id,createdBy,output,outputQuantity,item_1_id,item_1_quantity,item_2_id,item_2_quantity,item_3_id,item_3_quantity,item_4_id,item_4_quantity,lastUpdate'

    await defaultPath(database, req, res, collection, req.url, active, defaultFields, undefined, defaultFind)
    next()
  })

  // V2 recipes
  server.get('/gw2/v2/recipes', async (req, res, next) => {
    let defaultFields = 'id,chat_link,disciplines,firstAdded,flags,ingredients,min_rating,output_item_count,output_item_id,time_to_craft_ms,type,lastUpdate'

    await defaultPath(database, req, res, Recipes_Type, req.url, active, defaultFields, undefined, {id: {$gte:0}})
    next()
  })

  server.get('/gw2/v2/recipes/csv', async (req, res, next) => {
    let defaultFields = undefined
    let defaultHeaders = "true"

    const transform = (row) => {
      let result = {
        id: row.id,
        createdBy: row.disciplines.join(","),
        output: row.output_item_id,
        outputQuantity: row.output_item_count,
        lastUpdate: undefined
      }
      for(let i=0;i<5;i++){
        result["item_"+ (i+1) + "_id"] = ""
        result["item_"+ (i+1)  + "_quantity"] = ""

        if(typeof row.ingredients[i] !== "undefined"){
          result["item_"+  (i+1)  + "_id"] = row.ingredients[i].item_id
          result["item_"+  (i+1)  + "_quantity"] = row.ingredients[i].count
        }
        if(i === 3){
          result.lastUpdate = row.lastUpdate
        }
      }
      return result
    }

    active.project = false
    await defaultPath(database, req, res, Recipes_Type, req.url, active, defaultFields, 'csv', {id: {$gte:0}}, undefined, defaultHeaders, transform)
    next()
  })
}
