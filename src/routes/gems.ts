import {Ctx} from "../Gw2_API";
import {defaultPath} from '../JS/functions';
import {Gem_Price_Gold, Gem_Price_Gem} from "../config/db_entities"

export const Gems = (ctx: Ctx) =>  {
  const {database,server} = ctx;

  let active = { beautify: true, fields: true, project: true, filter: true, ids: true, itemID: false, start: false, end: false }
  let defaultFields = 'iso,price'


  server.get('/gw2/v1/gems/coins_to_gems/json', async (req, res, next) => {
    await defaultPath(database, req, res, Gem_Price_Gold, req.url, active, defaultFields, undefined, undefined, { iso: -1 })
    next()
  })

  server.get('/gw2/v1/gems/coins_to_gems/csv', async (req, res, next) => {
    await defaultPath(database, req, res, Gem_Price_Gold, req.url, active, defaultFields, "csv", undefined, { iso: -1 })
    next()
  })



  server.get('/gw2/v1/gems/gems_to_coins/json', async (req, res, next) => {
    await defaultPath(database, req, res, Gem_Price_Gem, req.url, active, defaultFields, undefined, undefined, { iso: -1 })
    next()
  })

  server.get('/gw2/v1/gems/gems_to_coins/csv', async (req, res, next) => {
    await defaultPath(database, req, res, Gem_Price_Gem, req.url, active, defaultFields, "csv", undefined, { iso: -1 })
    next()
  })
}
