import {Ctx} from "../Gw2_API";
import {defaultPath, beautifyJSON} from '../JS/functions';
import restify from "restify"
import {Market_Total, Market_Day, Market_Hour} from "../config/db_entities"

export const History = (ctx: Ctx) =>  {
  const {database,server} = ctx;

  let defaultFields = '' +
    'count,date,itemID,type,' +
    'buy_delisted,buy_listed,buy_sold,buy_value,' +
    'buy_price,buy_price_avg,buy_price_max,buy_price_min,buy_price_stdev,' +
    'buy_quantity,buy_quantity_avg,buy_quantity_max,buy_quantity_min,buy_quantity_stdev,' +
    'sell_delisted,sell_listed,sell_sold,sell_value,' +
    'sell_price,sell_price_avg,sell_price_max,sell_price_min,sell_price_stdev,' +
    'sell_quantity,sell_quantity_avg,sell_quantity_max,sell_quantity_min,sell_quantity_stdev,' +
    ''
  let active = { beautify: true, fields: false, project: true, filter: true, ids: false, itemID: true, start: true, end: true }

  server.get('/gw2/v1/history', async (req, res, next) => {
    await defaultPath(database, req, res, Market_Day, req.url, active, defaultFields, "history", {itemID:0}, sorter(req))
    next()
  })

  server.get('/gw2/v2/history/json', async (req, res, next) => {
    await defaultPath(database, req, res, Market_Day, req.url, active, defaultFields, "history", {itemID:0}, sorter(req))
    next()
  })

  server.get('/gw2/v2/history/csv', async (req, res, next) => {
    await defaultPath(database, req, res, Market_Day, req.url, active, defaultFields, 'csv', {itemID:0}, sorter(req))
    next()
  })

  server.get('/gw2/v2/history/hourly/json', async (req, res, next) => {
    await defaultPath(database, req, res, Market_Hour, req.url, active, defaultFields, "history", {itemID:0}, sorter(req))
    next()
  })
  server.get('/gw2/v2/history/hourly/csv', async (req, res, next) => {
    await defaultPath(database, req, res, Market_Hour, req.url, active, defaultFields, "csv", {itemID:0}, sorter(req))
    next()
  })

  server.get('/gw2/v2/history/day/json', async (req, res, next) => {
    let day = req.query["date"] || new Date().toISOString();
    let day_start = day.substring(0,10);

    // some earlier entries have dates ending in all zeros
    let query = { $or: [ { date: `${day_start}T00:00:01.000Z` }, { date: `${day_start}T00:00:00.000Z` } ] };

    let result = await database.get_items(Market_Day, query, undefined, 0, 0, {itemID: 1});
    res.sendRaw(200, beautifyJSON(result, "human"), { 'Content-Type': 'application/json; charset=utf-8' })
    next()
  })

  let totalFields = "date,count," +
    "buy_delisted,buy_listed,buy_sold,buy_value," +
    "sell_delisted,sell_delisted_value,sell_listed,sell_sold,sell_value"

  let totalActive = { beautify: true, fields: true, project: true, filter: true, ids: false, itemID: false, start: true, end: true }

  server.get('/gw2/v2/history/total/json', async (req, res, next) => {
    await defaultPath(database, req, res, Market_Total, req.url, totalActive, totalFields , 'json', {}, sorter(req))
    next()
  })

  server.get('/gw2/v2/history/total/csv', async (req, res, next) => {
    await defaultPath(database, req, res, Market_Total, req.url, totalActive, totalFields , 'csv', {}, sorter(req))
    next()
  })
}

function sorter(req: restify.Request){
  let defaultSort = {date: 1}
  if(!req.params.sorting){return defaultSort}

  let sorting = req.params.sorting.toString().toLowerCase()
  switch (sorting) {
    case 'new': { defaultSort['date'] = -1; break }
    case 'old': { defaultSort['date'] = 1; break }
    default: { defaultSort['date'] = 1 }
  }

  return defaultSort
}