import {Ctx} from "../Gw2_API";
import {defaultPath} from '../JS/functions';
import {Build as Build_Type} from "../config/db_entities"

export const Build = (ctx: Ctx) =>  {
  const {database,server} = ctx;

  let active = { beautify: true, fields: true, project: true, filter: true, ids: true, itemID: false, start: false, end: false }
  let defaultFields = 'id,iso'

  server.get('/gw2/v1/build/json', async (req, res, next) => {
    await defaultPath(database, req, res, Build_Type, req.url, active, defaultFields, undefined, undefined, { id: -1 })
    next()
  })
}
