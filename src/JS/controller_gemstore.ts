/*
  This handles everything that needs to update when the build changes
*/

let cheerio = require('cheerio');
import {array_to_keyed_object} from "./functions"
//import { itemGems } from "gw2e-item-value";
import Proxy from "../classes/Proxy"
import { DB_Manager } from "../config/db"
import { Gem_Store_Catalog, Gem_Store_Categories, Gem_Store_Index } from "../config/db_entities"
import { db } from "../config/config"

const proxy = new Proxy()

controller().then(() => console.log("Complete")).catch((err) => console.log(err))
async function controller () {
  let dbManager = new DB_Manager();
  await dbManager.lift(db.uri, db.database)

  await get_gem_store(dbManager);

  await dbManager.down();
}


async function get_gem_store(dbManager: DB_Manager){
  // can get out of sync with the actual buildID  of the game so set an override
  let build_id = 9999999;

  // first grab the page
  let build_page = await proxy.get(`https://gemstore-fra-live.ncplatform.net/?language=en&gamecode=gw2&buildid=${build_id}`).catch((err) => console.log(err));

  if (typeof build_page === 'undefined' || build_page.status !== 200 || typeof build_page.data === 'undefined') {
    return null
  }

  // get the links to each sub page
  let [categoriesUrl, catalogUrl, searchIndexUrl] = getDataUrls(build_page.data)

  await processCatalog(dbManager,catalogUrl)

  await processCategories(dbManager,categoriesUrl)

  await processSearchIndex(dbManager, searchIndexUrl, build_id)
}


interface Cleaned_Result<T> {
  [propName: string]:Partial<T>
}

async function processCatalog(dbManager: DB_Manager, url: string){
  let items: Partial<Gem_Store_Catalog>[] = []

  let catalog = await proxy.get(url).catch((err) => console.log(err));

  if (typeof catalog === 'undefined' || catalog.status !== 200 || typeof catalog.data === 'undefined') {
    return null
  }

  let existing_catalog = await dbManager.get_items<Gem_Store_Catalog>(Gem_Store_Catalog, {})
      .then((results) => array_to_keyed_object(results, "uuid"))
      .catch(err => {console.log(err); return {}})


  let itemsRaw : Cleaned_Result<Gem_Store_Catalog> = scriptToJson(catalog.data)
  let serials =  Object.keys(itemsRaw)

  for(let i=0;i<serials.length;i++){
    let uuid = serials[i]

    let item = itemsRaw[uuid]
    let existingItem = existing_catalog[uuid] || {} as Gem_Store_Catalog;

    // lifespans
    if(typeof existingItem.categoryLifespansOld === "undefined"){existingItem.categoryLifespansOld = []}
    item.categoryLifespansOld = [...existingItem.categoryLifespansOld]
    if(typeof item.categoryLifespans !== "undefined"){
      let categories = Object.keys(item.categoryLifespans)
      for(let j=0;j<categories.length;j++){
        let selectedCategory = item.categoryLifespans[categories[j]]
        for(let k=0;k<selectedCategory.length;k++){
          item.categoryLifespansOld.push({
            category: categories[j],
            start: selectedCategory[k].start,
            end: selectedCategory[k].end
          })
        }
      }
    }
    item.categoryLifespansOld = item.categoryLifespansOld.filter((v,i,a)=>a.findIndex(t=>(JSON.stringify(t) === JSON.stringify(v)))===i)


    item.latestStartDate =  existingItem.latestStartDate || undefined
    item.latestEndDate =  existingItem.latestEndDate || undefined
    if(item.categoryLifespansOld.length >0){
      let sorted = item.categoryLifespansOld.sort(function(a, b) {if (a.start < b.start) {return -1}else if (a.start > b.start) {return 1}else{return 0}}).reverse()
      // this will always be the latest
      item.latestEndDate = sorted[0].end

      // set this to the latest start date
      item.latestStartDate = sorted[0].start

      for(let j=1;j<sorted.length;j++){
        // checks if teh item is available but changed categories in said timeframe
        if(sorted[j-1].start === sorted[j].end){
          item.latestStartDate = sorted[j].start
        }else{
          break
        }
      }
    }


    // archiving categories
    if(typeof existingItem.categoriesOld === "undefined"){existingItem.categoriesOld = []}
    item.categoriesOld = [...existingItem.categoriesOld]

    if(typeof item.categories !== "undefined"){item.categoriesOld.push(...item.categories)}
    item.categoriesOld = item.categoriesOld.filter((v,i,a)=>a.findIndex(t=>(JSON.stringify(t) === JSON.stringify(v)))===i)

    if(typeof item.dataId !== "undefined"){
      /*
      let gemPrice = itemGems(item.dataId)
      if(gemPrice){
        item.gemPrice = gemPrice.gems
      }
       */
    }

    item.uuid = uuid
    items.push(item)
  }

  // why the double? in case the gem price changed since the last run
  let tmpItems = {}
  for(let i=0;i<items.length;i++){
    tmpItems[items[i].uuid] = items[i]
  }
  for(let i=0;i<items.length;i++){
    if(typeof items[i].contents === "undefined"){continue}
    let gemValue = 0

    for(let j=0;j<items[i].contents.length;j++){
      let itemDetails = tmpItems[items[i].contents[j].guid]
      if(typeof itemDetails === "undefined"){continue}
      if(typeof itemDetails.gemPrice !== "undefined"){
        gemValue += itemDetails.gemPrice
      }
    }

    if(gemValue >0){
      items[i].gemPrice = gemValue
    }
  }

  await dbManager.update_bulk(Gem_Store_Catalog, items,undefined, "uuid" );
}

async function processCategories(dbManager: DB_Manager, url: string){
  let categories = await proxy.get(url).catch((err) => console.log(err));

  if (typeof categories === 'undefined' || categories.status !== 200 || typeof categories.data === 'undefined') {
    return null
  }

  let categoriesRaw: Cleaned_Result<Gem_Store_Categories> = scriptToJson(categories.data)
  let serials =  Object.keys(categoriesRaw)

  let items: Partial<Gem_Store_Categories>[] = []
  for(let i=0;i<serials.length;i++){
    let uuid = serials[i]
    let item = categoriesRaw[uuid]
    item.uuid = uuid
    items.push(item)
  }

  await dbManager.update_bulk(Gem_Store_Categories, items, undefined, "uuid");
}

async function processSearchIndex(dbManager: DB_Manager, url: string, build_id: number){
  let categories = await proxy.get(url).catch((err) => console.log(err));

  if (typeof categories === 'undefined' || categories.status !== 200 || typeof categories.data === 'undefined') {
    return null
  }

  let itemsRaw: {guids: string[]} = scriptToJson(categories.data)

  let data: Gem_Store_Index = {
    build: build_id,
    items: itemsRaw.guids
  }

  await dbManager.update_bulk(Gem_Store_Index, [data], undefined, "build");
}

function getDataUrls(body): string[]{
  let categories, catalog, searchIndex;
  let $ = cheerio.load(body);
  let scripts = $('script').get()
  for(let i=0;i<scripts.length;i++){
    let url = scripts[i].attribs['src']
    if(typeof url === "undefined"){continue}
    if(url.indexOf("build")!== -1){
      if(url.indexOf("categories") !== -1){
        categories = url
      }
      if(url.indexOf("catalog") !== -1){
        catalog = url
      }
      if(url.indexOf("searchindex") !== -1){
        searchIndex = url
      }
    }
  }

  return [categories, catalog, searchIndex]
}

// cleans up the individual script
function scriptToJson(body) {
  return JSON.parse(
      body
          .replace("// automatically generated", "")
          .replace("var gemstoreCatalog = ", "")
          .replace("var gemstoreCategories = ", "")
          .replace("var gemstoreIndex = ", "")
  )
}