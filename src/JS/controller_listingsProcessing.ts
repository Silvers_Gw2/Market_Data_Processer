import {All_Info, Market_Minute, Market_Hour, Market_Base, Market_Day, Market_Total} from "../config/db_entities";
import {cutoffPercentages, db} from "../config/config"
import {dateAgo, chunkArray, sorter, arrayAvg, getSD} from "./functions";

import { DB_Manager } from "../config/db"

let startTime = new Date().getTime()
let oneDayAgo = dateAgo("hours", 24)
let twoDayAgo = dateAgo("hours", 24 * 2)
let sevenDayAgo = dateAgo("hours", 24 * 7)

let month = 30
let oneMonthAgo = dateAgo("days", month)
let threeMonthAgo = dateAgo("days", month * 3)
let twelveMonthAgo = dateAgo("days", month * 12)

let keys = [
  "sell_listed", "sell_delisted", "sell_sold",
  "buy_listed","buy_delisted","buy_sold",
  "sell_value", "buy_value" ,
  "buy_price_min","sell_price_min","buy_quantity_min", "sell_quantity_min",
  "buy_price_avg","sell_price_avg","buy_quantity_avg", "sell_quantity_avg",
  "buy_price_max","sell_price_max","buy_quantity_max", "sell_quantity_max",
  "sell_delisted_value",
]
let fields = ["buy_price", "sell_price","buy_quantity", "sell_quantity" ]

controller(process.argv[2]).catch((err) => console.log(err));

// .catch((err) => {console.log(err);return []})
async function controller (group: string = "min") {
  console.time("Total");
  let dbManager = new DB_Manager();
  await dbManager.lift(db.uri, db.database)

  let ids: number[] = await dbManager.get_items(All_Info, {marketable: true}, ["id"]).then(result => result.map(item => item.id)).catch(err => {console.log(err); return []});

  //ids = [19721,24]

  switch (group) {
    case "min":{
      await processMinToHours(dbManager, ids)
      await dbManager.delete_bulk(Market_Minute, { deleteDate: { $lt: startTime } })

      break
    }
    case "hour":{
      await processHoursToDays(dbManager, ids)
      await dbManager.delete_bulk(Market_Hour, { deleteDate: { $lt: startTime } })

      break
    }

    case "day":{
      await processDaysToMonths(dbManager, ids)
      break
    }
    default: {
      break
    }
  }

  await dbManager.down();
  console.timeEnd("Total");
}

interface Market_Merged extends Market_Base {
  buy_price: number;
  sell_price: number;
  buy_quantity: number;
  sell_quantity: number;


  buy_quantity_array_min_max?: number[];
  buy_quantity_array_avg_sd?: number[];
  
  buy_price_array_min_max?: number[];
  buy_price_array_avg_sd?: number[];
  
  sell_price_array_min_max?: number[];
  sell_price_array_avg_sd?: number[];
  
  sell_quantity_array_min_max?: number[];
  sell_quantity_array_avg_sd?: number[];

  deleteDate?: number;
  keep: boolean;
}
interface Keyed_Market_Merged {
  [propName: string]: Partial<Market_Merged>
}

interface Market_Total_Merged {
  [propName: string]: Market_Total
}

function calculateSoldController (tmp: Keyed_Market_Merged, accessor: string, currentMarketData: Market_Minute, lastMarketData: Market_Minute, progressionValues: string[], category: string): Keyed_Market_Merged{
  let totalChanged = currentMarketData[category + "_quantity"] - lastMarketData[category + "_quantity"]
  let cutoffChange = 0
  let cutoffValue
  for (let j = 0; j < progressionValues.length; j++) {
    // before
    if (typeof lastMarketData[category + "_cutoff_qty_before"][progressionValues[j]] === "undefined") { continue }
    // after
    if (typeof currentMarketData[category + "_cutoff_qty_after"][progressionValues[j]] === "undefined") { continue }
    if (currentMarketData[category + "_cutoff_qty_after"][progressionValues[j]] === 0) { continue }

    if(typeof lastMarketData[category + "_cutoff_price_before"] !== "undefined" && typeof currentMarketData[category + "_cutoff_price_after"] !== "undefined"){
      if(lastMarketData[category + "_cutoff_price_before"][progressionValues[j]] !== currentMarketData[category + "_cutoff_price_after"][progressionValues[j]]){continue}
    }

	  cutoffValue = progressionValues[j]
	  cutoffChange = currentMarketData[category + "_cutoff_qty_after"][progressionValues[j]] - lastMarketData[category + "_cutoff_qty_before"][progressionValues[j]]
		//console.log(cutoffValue, cutoffChange)
	  // try again
	  if(cutoffChange === 0){continue}

	  break
  }

  let sold, listed, delisted
  if(typeof cutoffValue === "undefined"){
    sold = 0
    listed = 0
    delisted = 0
  }else{
    [sold, listed, delisted] = calculateSold(totalChanged, cutoffChange)
  }

  tmp[accessor][category + "_listed"] += listed
  tmp[accessor][category + "_delisted"] += delisted
  tmp[accessor][category + "_sold"] += sold
  tmp[accessor][category + "_value"] += (sold * currentMarketData[category + "_price"]) || 0

  if(category === "sell"){
    tmp[accessor][category + "_delisted_value"] += (delisted * currentMarketData[category + "_price"])
  }

  return tmp
}

export function calculateSold (total, cutoff) {
  let listed = 0
  let delisted = 0
  let sold = 0

  // if total is negative then there is a reduced amount if items / sold + delisted
  if (total <= 0) {
    // then if cutoff is negative then roughly that many sold
    if (cutoff <= 0) {
      // then if cutoff is negative then roughly that many sold
      sold += (cutoff * -1)

      if (total > cutoff) {
        // if more sold than the total change then some were listed
        listed += (cutoff * -1) + total
      } else {
        delisted += (total * -1) - (cutoff * -1)
      }
    }

    if (cutoff > 0) {
      if ((total *-1) > cutoff) {
        listed += (total * -1)
        delisted += (total * -1) + cutoff
      } else {
        listed += cutoff
        delisted += (total * -1) + cutoff
      }
    }
  }

  // if total is positive then there is an increased amount if items
  if (total > 0) {
    if (cutoff <= 0) {
      // then if cutoff is negative then roughly that many sold
      sold += (cutoff * -1)
      // then if that many sold then there more listed
      listed += total - cutoff
    }

    if (cutoff > 0) {
      // cutoff is positive then there were some listed
      listed += cutoff

      if (total < cutoff) {
        // if more listed than the total change then some were delisted
        delisted += cutoff - total
      } else {
        listed += total - cutoff
      }
    }
  }
  return [sold, listed, delisted]
}

function compress_minute (marketData: Market_Minute[]): Partial<Market_Merged>[] {
  let tempData: Keyed_Market_Merged = {}
  for (let i = 0; i < marketData.length; i++) {
    let itemDate = marketData[i].date
    let minuteID = itemDate + '_' + marketData[i].itemID + "_minute"
    if (typeof tempData[minuteID] === 'undefined') {
      tempData[minuteID] = {
        id: minuteID,
        itemID: marketData[i].itemID,
        count: 0,
        date: itemDate,
        keep: false,

        buy_count: 0,
        sell_count: 0,
        buy_quantity: 0,
        buy_price: 0,
        sell_quantity: 0,
        sell_price: 0,

        sell_listed: 0,
        sell_delisted: 0,
        sell_delisted_value: 0,
        sell_sold: 0,
        buy_listed: 0,
        buy_delisted: 0,
        buy_sold: 0,

        sell_value: 0,
        buy_value: 0,
      }
    }

    tempData[minuteID].count += 1

    if(marketData[i].buy_quantity){
      tempData[minuteID].buy_count += 1
      tempData[minuteID].buy_quantity += marketData[i].buy_quantity
      tempData[minuteID].buy_price += marketData[i].buy_price
    }
    if(marketData[i].sell_quantity){
      tempData[minuteID].sell_count += 1
      tempData[minuteID].sell_quantity += marketData[i].sell_quantity
      tempData[minuteID].sell_price += marketData[i].sell_price
    }

    if (typeof marketData[i - 1] !== 'undefined') {
      tempData[minuteID].keep = true
      // Offers
      tempData = calculateSoldController(tempData, minuteID, marketData[i], marketData[i - 1], cutoffPercentages, "sell")

      // requests
      tempData = calculateSoldController(tempData, minuteID, marketData[i], marketData[i - 1], cutoffPercentages, "buy")
    }
  }

  let tmp: Partial<Market_Merged>[] = []

  let items = Object.values(tempData)
  for (let i = 0; i < items.length; i++) {
    if(items[i].keep){
      tmp.push(items[i])
    }
  }
  return tmp
}

function mergeSmaller(tmp: Keyed_Market_Merged, toMerge: Partial<Market_Merged>, template, accessor: string, date: string, deleteDate?: number): Keyed_Market_Merged{
  tmp = Object.assign({}, tmp)
  toMerge = Object.assign({}, toMerge)
  if (typeof tmp[accessor] === 'undefined') {
    tmp[accessor] = {
      id: accessor,
      itemID: toMerge.itemID,
      date: date,
      count: 0,
      buy_count: 0,
      sell_count: 0,

      buy_quantity: 0,
      buy_price: 0,
      sell_quantity: 0,
      sell_price: 0,

      sell_listed: 0,
      sell_delisted: 0,
      sell_delisted_value: 0,
      sell_sold: 0,
      buy_listed: 0,
      buy_delisted: 0,
      buy_sold: 0,

      sell_value: 0,
      buy_value: 0,

      buy_quantity_array_min_max: [],
      buy_quantity_array_avg_sd: [],

      buy_price_array_min_max: [],
      buy_price_array_avg_sd: [],

      sell_price_array_min_max: [],
      sell_price_array_avg_sd: [],

      sell_quantity_array_min_max: [],
      sell_quantity_array_avg_sd: [],
    }
    if(deleteDate){
      tmp[accessor].deleteDate = deleteDate;
    }
  }

  let mergeKeys = Object.keys(toMerge)
  for(let j=0;j<mergeKeys.length;j++){
    if(
      mergeKeys[j] === "itemID"
      || mergeKeys[j] === "id"
      || mergeKeys[j] === "date"
      || mergeKeys[j] === "type"
      || mergeKeys[j] === "deleteDate"
    ){
      continue
    }
    let value = toMerge[mergeKeys[j]]
    // only deal in numbers from ehre on out
    if(isNaN(value) || !isFinite(value) || value === 0){
      continue
    }

    if(
      mergeKeys[j] === "buy_quantity"
      || mergeKeys[j] === "buy_price"
      || mergeKeys[j] === "sell_quantity"
      || mergeKeys[j] === "sell_price"
    ){
      if(mergeKeys[j] === "buy_quantity" || mergeKeys[j] === "buy_price"){
        if(toMerge["buy_quantity"] === 0){ continue }
      }
      if(mergeKeys[j] === "sell_quantity" || mergeKeys[j] === "sell_price"){
        if(toMerge["sell_quantity"] === 0){ continue }
      }

      if(typeof value === "number"){
        // push it into the array to hold it temp
        tmp[accessor][`${mergeKeys[j]}_array_min_max`].push(value)
        tmp[accessor][`${mergeKeys[j]}_array_avg_sd`].push(value)
      }
      if(typeof value=== "object"){
        for(let k=0;k<value.length;k++){
          if(typeof value[k] === "number"){
            tmp[accessor][mergeKeys[j]].push(value[k])
          }
        }
      }

      continue
    }

    if(
      mergeKeys[j] === "count"
        || mergeKeys[j] === "buy_count"
        || mergeKeys[j] === "sell_count"
      || mergeKeys[j] === "sell_listed"
      || mergeKeys[j] === "sell_delisted"
      || mergeKeys[j] === "sell_sold"
      || mergeKeys[j] === "buy_listed"
      || mergeKeys[j] === "buy_delisted"
      || mergeKeys[j] === "buy_sold"
      || mergeKeys[j] === "sell_value"
      || mergeKeys[j] === "buy_value"
      || mergeKeys[j] === "sell_delisted_value"
    ){
      if(typeof value === "number"){
        tmp[accessor][mergeKeys[j]] += value
        continue
      }
    }
    if(mergeKeys[j].indexOf("_min") !== -1){
      let newKey = mergeKeys[j].replace("_min","_array_min_max")
      tmp[accessor][newKey].push(value)
      continue
    }
    if(mergeKeys[j].indexOf("_max") !== -1){
      let newKey = mergeKeys[j].replace("_max","_array_min_max")
      tmp[accessor][newKey].push(value)
      continue
    }
    if(mergeKeys[j].indexOf("_avg") !== -1){
      let newKey = mergeKeys[j].replace("_avg","_array_avg_sd")
      tmp[accessor][newKey].push(value)
      //continue
    }

  }
  return tmp
}

function minMaxAverage (allData, naming?: string) {
  let defaultNames = {
    min:"_min",
    max:"_max",
    avg:"_avg",
    std:"_stdev"
  }

  if(naming){
    let vars = ["min", "max", "avg", "std"]
    for(let i=0;i<vars.length;i++){
      if(typeof naming[vars[i]] !== "undefined"){
        defaultNames[vars[i]] = naming[vars[i]]
      }
    }
  }

  for(let i=0;i<fields.length;i++){
    if(typeof allData[`${fields[i]}_array_min_max`] !== "undefined"){
      allData[fields[i] + defaultNames.min] = Math.round(Math.min(...allData[`${fields[i]}_array_min_max`]))
      allData[fields[i] + defaultNames.max] = Math.round(Math.max(...allData[`${fields[i]}_array_min_max`]))
    }
    if(typeof allData[`${fields[i]}_array_avg_sd`] !== "undefined"){
      allData[fields[i] + defaultNames.avg] = Math.round(arrayAvg(allData[`${fields[i]}_array_avg_sd`]))
      allData[fields[i] + defaultNames.std] = Math.round(getSD(allData[`${fields[i]}_array_avg_sd`])*100)/100
    }

    delete allData[`${fields[i]}_array_min_max`]
    delete allData[`${fields[i]}_array_avg_sd`]
  }
  return allData
}

async function processMinToHours(dbManager: DB_Manager, ids: number[]){
  let idChunked = chunkArray (ids, 500)
  for(let i=0;i<idChunked.length;i++){
    console.log("processMinToHours", i,idChunked.length)
    let hours: Market_Hour[] = []
    let promises = idChunked[i].map(async (id) => {
      let tmp: Keyed_Market_Merged = {};

      let itemData = await dbManager.get_items(Market_Minute, {itemID: id}).then(result => result.sort((a,b)=> sorter(a.date, b.date)));

      // the data now has to be
      let compressed = compress_minute (itemData)
      for(let j=0;j<compressed.length;j++){
        let hourDate = dateAgo("hours", 0, compressed[j].date)
        let hourID = `${hourDate}_${compressed[j].itemID}_hour`
        tmp = mergeSmaller(tmp,compressed[j] , undefined, hourID, hourDate, new Date(dateAgo("days", -8)).getTime())
      }

      let items = Object.values(tmp)
      for(let j=0;j<items.length;j++){
        let item = minMaxAverage (items[j])
        for(let k=0;k<fields.length;k++){
          delete item[fields[k]]
        }
        hours.push(item)
      }
      
    })
    await Promise.allSettled(promises)

    await dbManager.update_bulk(Market_Hour, hours)
  }
}

async function processHoursToDays(dbManager: DB_Manager, ids: number[]){
  let idChunked = chunkArray (ids, 500)
  let totals: Market_Total_Merged = {}
  for(let i=0;i<idChunked.length;i++){
    console.log("processHoursToDays", i,idChunked.length)
    let days: Market_Day[] = []
    let allData: Partial<All_Info>[] = []
    let promises = idChunked[i].map(async (id) => {
      let tmp: Keyed_Market_Merged = {}
      let lastDay: Keyed_Market_Merged = {}

      let itemData = await dbManager.get_items(Market_Hour, {itemID: id}).then(result => result.sort((a,b)=> sorter(a.date, b.date)));

      for(let j=0;j<itemData.length;j++){
        let date = dateAgo("days", 0, itemData[j].date, 1)
        let entryID = date + '_' + id + "_day"
        tmp  = mergeSmaller(tmp,itemData[j] , undefined, entryID, date)

        if(itemData[j].date >= oneDayAgo){
          lastDay = mergeSmaller(lastDay,itemData[j] , undefined, "1d", date)
        }

        if(itemData[j].date >= twoDayAgo){
          lastDay = mergeSmaller(lastDay,itemData[j] , undefined, "2d", date)
        }

        if(itemData[j].date >= sevenDayAgo){
          lastDay = mergeSmaller(lastDay,itemData[j] , undefined, "7d", date)
        }

      }

      let items = Object.values(tmp)
      for(let j=0;j<items.length;j++){
        let item = minMaxAverage (items[j])
        for(let k=0;k<fields.length;k++){
          delete item[fields[k]]
        }
        days.push(item)
      }

      let dayKeys = Object.keys(lastDay)
      for(let j=0;j<dayKeys.length;j++){
        let day = dayKeys[j]
        if(typeof lastDay[day] === "undefined"){continue}
        let temp: Partial<All_Info> = {
          id:lastDay[day].itemID,
          lastUpdate: new Date().toISOString(),
        }

        lastDay[day] = minMaxAverage (lastDay[day])
        for(let k=0;k<keys.length;k++){
          temp[day+"_"+keys[k]] = lastDay[day][keys[k]]
        }
        allData.push(temp)
      }
    })
    await Promise.allSettled(promises)

    await dbManager.update_bulk(Market_Day, days)
    await dbManager.update_bulk(All_Info, allData, undefined, undefined, { firstAdded: new Date().toISOString() })

    totals = processTotals(totals, days)
  }
  let totalsArray: Market_Total[] = []
  let items = Object.values(totals)
  for(let i=0;i<items.length;i++){
    totalsArray.push(items[i])
  }
  await dbManager.update_bulk(Market_Total, totalsArray, undefined, "date")
}

async function processDaysToMonths(dbManager: DB_Manager, ids: number[]){
  let idChunked = chunkArray (ids, 500)
  for(let i=0;i<idChunked.length;i++){
    console.log("processDaysToMonths", i,idChunked.length)
    let allData: Partial<All_Info>[] = []
    let promises = idChunked[i].map(async (id) => {
      let lastDay: Keyed_Market_Merged = {}

      let itemData = await dbManager.get_items(Market_Day, {itemID: id, date:{$gte: twelveMonthAgo}}).then(result => result.sort((a,b)=> sorter(a.date, b.date)));

      for(let j=0;j<itemData.length;j++){
        let date = dateAgo("days", 0, itemData[j].date, 1)

        if(itemData[j].date >= oneMonthAgo){
          lastDay = mergeSmaller(lastDay,itemData[j] , undefined, "1m", date)
        }

        if(itemData[j].date >= threeMonthAgo){
          lastDay = mergeSmaller(lastDay,itemData[j] , undefined, "3m", date)
        }

        if(itemData[j].date >= twelveMonthAgo){
          lastDay = mergeSmaller(lastDay,itemData[j] , undefined, "12m", date)
        }
      }

      let dayKeys = Object.keys(lastDay)
      for(let j=0;j<dayKeys.length;j++){
        let day = dayKeys[j]
        if(typeof lastDay[day] === "undefined"){continue}
        let temp: Partial<All_Info> = {
          id:lastDay[day].itemID,
          lastUpdate: new Date().toISOString(),
        }

        lastDay[day] = minMaxAverage (lastDay[day])
        for(let k=0;k<keys.length;k++){
          temp[day+"_"+keys[k]] = lastDay[day][keys[k]]
        }
        allData.push(temp)
      }
    })
    await Promise.allSettled(promises)

    await dbManager.update_bulk(All_Info, allData, undefined, undefined, { firstAdded: new Date().toISOString() })
  }
}

function processTotals(totals: Market_Total_Merged, days: Market_Day[]): Market_Total_Merged{
  for(let i=0;i<days.length;i++){
    let date = days[i].date
    if(typeof totals[date] === "undefined"){
      totals[date] = {
        date: date,
        count : 0,
        sell_sold: 0,
        buy_sold: 0,
        sell_value: 0,
        buy_value: 0,
        sell_listed: 0,
        buy_listed: 0,
        sell_delisted: 0,
        buy_delisted: 0,
        sell_delisted_value: 0,
      }
    }

    totals[date].count += 1
    totals[date].sell_sold += days[i].sell_sold
    totals[date].buy_sold += days[i].buy_sold
    totals[date].sell_value += days[i].sell_value
    totals[date].buy_value += days[i].buy_value
    totals[date].sell_listed += days[i].sell_listed
    totals[date].buy_listed += days[i].buy_listed
    totals[date].sell_delisted += days[i].sell_delisted
    totals[date].buy_delisted += days[i].buy_delisted
    totals[date].sell_delisted_value += days[i].sell_delisted_value
  }
  return totals
}