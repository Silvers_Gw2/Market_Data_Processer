
import Proxy from "../classes/Proxy"
import { DB_Manager } from "../config/db"
import { Gem_Price_Gold, Gem_Price_Gem } from "../config/db_entities"
import { db } from "../config/config"

const proxy = new Proxy()

controller().then(() => console.log("Complete")).catch((err) => console.log(err))
async function controller () {
    let dbManager = new DB_Manager();
    await dbManager.lift(db.uri, db.database)

    await get_price_coins(dbManager);
    await get_price_gems(dbManager);

    await dbManager.down();
}


interface Price_Result {
    coins_per_gem: number;
    quantity: number;
}

// due to them being asymmetrical the quantities are different
// probably smarter ways to do it but this is pretty clear

async function get_price_coins(dbManager: DB_Manager){
    let result: Price_Result = await proxy.get(`https://api.guildwars2.com/v2/commerce/exchange/coins?quantity=100000`)
        .then((result)=> result.data )
        .catch((err) => {console.log(err); return {coins_per_gem: 0, quantity: 0}});

    if(result.coins_per_gem === 0){return}

    let tmp: Gem_Price_Gold = {
        iso: new Date().toISOString(),
        price: result.coins_per_gem
    }

    await dbManager.update_bulk(Gem_Price_Gold, [tmp], undefined, "iso")
}

async function get_price_gems(dbManager: DB_Manager){
    let result: Price_Result = await proxy.get(`https://api.guildwars2.com/v2/commerce/exchange/gems?quantity=1000`)
        .then((result)=> result.data )
        .catch((err) => {console.log(err); return {coins_per_gem: 0, quantity: 0}});

    if(result.coins_per_gem === 0){return}

    let tmp: Gem_Price_Gem = {
        iso: new Date().toISOString(),
        price: result.coins_per_gem
    }

    await dbManager.update_bulk(Gem_Price_Gem, [tmp], undefined, "iso")
}