/*
  This handles everything that needs to update when the build changes
*/

import Proxy from "../classes/Proxy"
import {DB_Manager} from "../config/db"
import {Build} from "../config/db_entities"
import {db} from "../config/config"

const proxy = new Proxy()

controller().then(() => console.log("Complete")).catch((err) => console.log(err))

async function controller() {
  // no https
  let build_result = await proxy.get(`http://assetcdn.101.arenanetworks.com/latest/101`).catch(err => {
    console.log(err);
    return err
  })
  if (typeof build_result === 'undefined' || build_result.status !== 200 || typeof build_result.data === 'undefined') {
    return
  }
  let build_data = build_result.data.split(" ");
  if (build_data.length < 0) {
    return
  }
  let build = parseInt(build_data[0], 10);

  let dbManager = new DB_Manager();
  await dbManager.lift(db.uri, db.database)

  let build_own = await dbManager.get_items(Build, {id: build})

  if (build_own.length === 0) {
    await dbManager.update_bulk(Build, [{id: build, iso: new Date().toISOString()}])
  }

  await dbManager.down();
}