/*
  This handles everything that needs to be updated, independent of build
*/

import {array_to_keyed_object, chunkArray, get_endpoint_data} from "./functions"
import Proxy from "../classes/Proxy"
import { DB_Manager } from "../config/db"
import { Achievements, Currencies, Item, Skins, Recipes, Item_Stats, All_Info } from "../config/db_entities"
import { db } from "../config/config"

const proxy = new Proxy()

let end_messages = [];

controller(process.argv[2]).catch((err) => console.log(err))

async function controller (option:string = "normal") {
  let timer = Date.now();

  let dbManager = new DB_Manager();
  await dbManager.lift(db.uri, db.database)

  if (option === "normal"){
    let full_refresh = false;
    // run in parallel
    let running = [
      controller_general(dbManager, "https://api.guildwars2.com/v2/achievements", "achievements", Achievements, 170, full_refresh).catch(err => console.log(err)),
      controller_general(dbManager, "https://api.guildwars2.com/v2/currencies", "currencies", Currencies, 170, full_refresh).catch(err => console.log(err)),
      controller_items(dbManager, "https://api.guildwars2.com/v2/items", "items", Item, 170, full_refresh).catch(err => console.log(err)),
      controller_general(dbManager, "https://api.guildwars2.com/v2/skins", "skins", Skins, 170, full_refresh).catch(err => console.log(err)),

      controller_recipes(dbManager, "https://api.guildwars2.com/v2/recipes", "recipes", Recipes, 170, full_refresh).catch(err => console.log(err)),
      controller_general(dbManager, "https://api.guildwars2.com/v2/itemstats", "item_stats", Item_Stats, 170, full_refresh).catch(err => console.log(err)),
    ]

    await Promise.allSettled(running).then(()=>{console.log("Incremental Complete")})
  }else{
    let full_refresh = true;
    // in series
    await controller_general(dbManager, "https://api.guildwars2.com/v2/achievements", "achievements", Achievements, 170, full_refresh)
    await controller_general(dbManager, "https://api.guildwars2.com/v2/currencies", "currencies", Currencies, 170, full_refresh)
    await controller_items(dbManager, "https://api.guildwars2.com/v2/items", "items", Item, 170, full_refresh)
    await controller_general(dbManager, "https://api.guildwars2.com/v2/skins", "skins", Skins, 170, full_refresh)

    await controller_recipes(dbManager, "https://api.guildwars2.com/v2/recipes", "recipes", Recipes, 170, full_refresh)
    await controller_general(dbManager, "https://api.guildwars2.com/v2/itemstats", "item_stats", Item_Stats, 170, full_refresh)
  }

  let total_time =  (Date.now() - timer) / 1000;
  end_messages.push({migration: "Total", items: 0, time: total_time})

  console.table(end_messages)

  await dbManager.down();
}

export async function controller_general<T>(dbManager:DB_Manager, baseURL: string, controller: string, table: new () => T, perQuery: number, full_refresh?:boolean){
  let timer = Date.now();

  // this gets both the previous ID's and the latest from teh api
  let idStr = await get_id_strings<T>(dbManager, table, baseURL, perQuery, full_refresh);

  let results = await get_endpoint_data<T> (idStr, baseURL, proxy);

  await dbManager.update_bulk<T>(table, results, undefined, undefined, undefined, new Date().toISOString());

  let total_time =  (Date.now() - timer) / 1000;

  // to be outputted when the individual migration is finished
  console.log(`${controller}: ${results.length} items in ${total_time}s`)

  // to be displayed at the end as a table
  end_messages.push({migration: controller, items: results.length, time: total_time})
}

export async function controller_recipes(dbManager:DB_Manager, baseURL: string, controller: string, table: new () => Recipes, perQuery: number, full_refresh?:boolean){
  let timer = Date.now();

  // this gets both the previous ID's and the latest from teh api
  let idStr = await get_id_strings<Recipes>(dbManager, table, baseURL, perQuery, full_refresh);

  let results = await get_endpoint_data<Recipes> (idStr, baseURL, proxy);

  await dbManager.update_bulk<Recipes>(table, results, undefined, undefined, undefined, new Date().toISOString());

  let total_time = (Date.now() - timer) / 1000;

  // to be outputted when the individual migration is finished
  console.log(`${controller}: ${results.length} items in ${total_time}s`)

  // to be displayed at the end as a table
  end_messages.push({migration: controller, items: results.length, time: total_time})

  timer = Date.now();

  // now take the recipes, get a list of ids that are outputted and save it in All_Info
  let results_all_info = results.map((item) => {
    let tmp: Partial<All_Info> = {
      id: item.output_item_id,
      craftable: true,
    }
    return tmp;
  })

  await dbManager.update_bulk(All_Info, results_all_info , undefined, undefined, { firstAdded: new Date().toISOString() });

  let total_time_all_info = (Date.now() - timer) / 1000;
  console.log(`${controller}_all_info: ${results.length} items in ${total_time_all_info}s`)
  end_messages.push({migration: `${controller}_all_info`, items: results.length, time: total_time_all_info})
}

export async function controller_items<T>(dbManager:DB_Manager, baseURL: string, controller: string, table: new () => T, perQuery: number, full_refresh?:boolean){
  // virtually the same as the other one but just for items, to also get localisation data

  let timer = Date.now();

  // this gets both the previous ID's and the latest from teh api
  let idStr = await get_id_strings<T>(dbManager, table, baseURL, perQuery, full_refresh);

  // general item info
  let results = await get_endpoint_data<T> (idStr, baseURL, proxy);

  await dbManager.update_bulk<T>(table, results, undefined, undefined, undefined, new Date().toISOString());

  let main_time =  (Date.now() - timer) / 1000;
  console.log(`${controller}_main: ${results.length} items in ${main_time}s`)
  end_messages.push({migration: `${controller}_main`, items: results.length, time: main_time})


  // locale now
  let timer_locale = Date.now();
  let results_locale = await get_endpoint_data_locale(idStr, baseURL);

  // add to all_info
  await dbManager.update_bulk<All_Info>(All_Info, results_locale, undefined, undefined, { firstAdded: new Date().toISOString() }, new Date().toISOString());
  let locale_time =  (Date.now() - timer_locale) / 1000;
  console.log(`${controller}_locale: ${results_locale.length} items in ${locale_time}s`)
  end_messages.push({migration: `${controller}_locale`, items: results_locale.length, time: locale_time})


  let total_time =  (Date.now() - timer) / 1000;
  end_messages.push({migration: controller, items: results.length, time: total_time})
}

// this gets the ids and puts them in teh correct format
export async function get_id_strings<T>(dbManager: DB_Manager, table: new () => T, baseURL: string, perQuery: number, full_refresh?:boolean): Promise<string[]>{
  // get current
  let existing;
  if (!full_refresh){
    // when not a full refresh only get teh enw items
    existing = await dbManager.get_items<T>(table, {}, ["id"])
        .then((results) => array_to_keyed_object(results, "id"))
        .catch(err => {console.log(err); return {}})
  }else{
    // on a full refresh grab all items
    existing = {};
  }


  // get new
  let initial = await proxy.get(baseURL).catch(err => {console.log(err); return err})
  if (typeof initial === 'undefined' || initial.status !== 200 || typeof initial.data === 'undefined' || typeof initial.data.text !== 'undefined') {
    // provide blank here
    initial = {data: []}
  }

  let ids = [];
  // merge
  let ids_tmp: number[] = initial.data
  for (let i = 0; i < ids_tmp.length; i++) {
    if(typeof existing[ids_tmp[i]] === "undefined"){
      ids.push(ids_tmp[i])
    }
  }

  let idsStr = []
  let tmp = []
  for (let i = 0; i < ids.length; i++) {
    if (tmp.length < perQuery) {
      tmp.push(ids[i])
    } else {
      idsStr.push(tmp.join(','))
      tmp = [ids[i]]
    }
  }
  if (tmp.length > 0) {
    idsStr.push(tmp.join(','))
  }

  return idsStr;
}

export async function get_endpoint_data_locale(ids_strings: string[], baseURL:string):Promise<Partial<All_Info>[]> {
  let locales = ["es", "de", "fr"]
  let details:Partial<All_Info>[] = []

  let chunked = chunkArray (ids_strings, 20)

  for (let j=0;j<locales.length; j++) {
    let lang = locales[j];
    for (let i = 0; i < chunked.length; i++) {
      let promises = chunked[i].map(async (idStr) => {
        let result: Item[] = await proxy.get(`${baseURL}?ids=${idStr}&lang=${lang}`).then(result => result.data).catch(err => {console.log(err);return []})
        for (let k = 0; k < result.length; k++) {
          let {id, name, description} = result[k];

          let tmp: Partial<All_Info> = {id: id}
          tmp[`name_${lang}`] = name
          tmp[`description_${lang}`] = description

          details.push(tmp)
        }
      })

      await Promise.allSettled(promises)
    }
  }

  return details
}