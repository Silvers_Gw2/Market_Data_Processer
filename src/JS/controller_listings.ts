
import { dateAgo, sorter, array_to_keyed_object, get_endpoint_data } from "./functions"
import { DB_Manager } from "../config/db"
import { All_Info, Cutoff, All_Info_Snipe_It, Market_Minute } from "../config/db_entities"
import {cutoffPercentages, db } from "../config/config";
import Proxy from "../classes/Proxy";

const proxy = new Proxy()

interface Listings_API_SubListings {
  listings: number;
  unit_price: number;
  quantity: number;
}

interface Listings_API {
  id: number;
  buys: Listings_API_SubListings [];
  sells: Listings_API_SubListings [];
}

let end_messages = [];
main().then(() => console.log("Complete")).catch((err) => console.log(err));
async function main(){
  let baseURL = "https://api.guildwars2.com/v2/commerce/listings";
  let perQuery = 175;

  let timer = Date.now()

  let dbManager = new DB_Manager();
  await dbManager.lift(db.uri, db.database)

  let existingItems = await dbManager.get_items(All_Info, {marketable: true}, ["id", "month_sell_max", "month_sell_avg", "week_sell_avg", "buy_price", "sell_price", "buy_cutoff_price", "sell_cutoff_price"])

  let idStr_test = await get_id_strings(baseURL, perQuery);

  let data_raw = await get_endpoint_data<Listings_API> (idStr_test, baseURL, proxy);

  let {all_info, market_data} = process_data_raw_controller(data_raw, existingItems);

  await dbManager.update_bulk(All_Info, all_info, undefined, undefined, { firstAdded: new Date().toISOString() });
  await dbManager.update_bulk(Market_Minute, market_data);

  let total_time =  (Date.now() - timer) / 1000;
  end_messages.push({migration: "Total", items: data_raw.length, time: total_time})

  console.table(end_messages)

  await dbManager.down();
}

// this gets the ids and puts them in teh correct format
export async function get_id_strings<T>(baseURL: string, perQuery: number): Promise<string[]>{
  // get new
  let initial = await proxy.get(baseURL).catch(err => {console.log(err); return {data: [], status: 500}})
  if (typeof initial === 'undefined' || initial.status !== 200 || typeof initial.data === 'undefined' || typeof initial.data.text !== 'undefined') {
    // provide blank here
    initial = {data: [], status: 500}
  }

  // merge
  let ids: number[] = initial.data

  let idsStr = []
  let tmp = []
  for (let i = 0; i < ids.length; i++) {
    if (tmp.length < perQuery) {
      tmp.push(ids[i])
    } else {
      idsStr.push(tmp.join(','))
      tmp = [ids[i]]
    }
  }
  if (tmp.length > 0) {
    idsStr.push(tmp.join(','))
  }

  return idsStr;
}

function process_data_raw_controller (data_raw: Listings_API[], existingItems: All_Info[]): { all_info: Partial<All_Info>[], market_data: Market_Minute[] } {
  let all_info_array: Partial<All_Info>[] = [];
  let market_data_array = [];

  let existing_keyed = array_to_keyed_object(existingItems, "id")

  for(let i=0;i< data_raw.length;i++){
    let tmp = process_data_raw(data_raw[i], existing_keyed);
    if(tmp !== null){
      all_info_array.push(tmp.data_AllData);
      market_data_array.push(tmp.data_market);
    }
  }

  return {
    all_info: all_info_array,
    market_data: market_data_array,
  }
}

interface All_Info_Snipe_It_Tmp extends All_Info_Snipe_It{
  profit: number;
  sorting: number;
}

function get_progression_defaults (): Cutoff {
  return {
    "0": 0,
    "0.0005": 0,
    "0.001": 0,
    "0.005": 0,
    "0.01": 0,
    "0.015": 0,
    "0.02": 0,
    "0.05": 0,
    "0.1": 0,
    "0.2": 0,
    "0.25": 0,
    "0.3": 0,
    "0.4": 0,
    "0.5": 0
  }
}

function process_data_raw (dataRaw: Listings_API, existingItems: {[p: string]: All_Info}): { data_AllData: Partial<All_Info>, data_market: Market_Minute} {
  // just in case
  if (typeof dataRaw === 'undefined') {
    return null;
  }
  if (typeof dataRaw.sells === 'undefined') {
    return null;
  }
  if (typeof dataRaw.buys === 'undefined') {
    return null;
  }
  if (typeof dataRaw.id === 'undefined') {
    return null;
  }

  let tmp: Partial<All_Info> = {}
  tmp.id = dataRaw.id
  tmp.marketable = true
  tmp.AccountBound = false
  tmp.lastUpdate = new Date().toISOString()

  let progressionValues = cutoffPercentages.map(number => parseFloat(number))

  let existing_item = existingItems[dataRaw.id];
  let ownData;
  if(existing_item){
    ownData = { 
      buy_cutoff_price: existing_item.buy_cutoff_price, 
      sell_cutoff_price: existing_item.sell_cutoff_price
    };
  }else{
    ownData = {
      buy_cutoff_price: get_progression_defaults(),
      sell_cutoff_price: get_progression_defaults()
    };
  }

  // regular vars
  tmp.buy_quantity = 0;
  tmp.buy_price = 0;
  tmp.buy_listings = 0;
  tmp.sell_quantity = 0;
  tmp.sell_price = 0;
  tmp.sell_listings = 0;

  // for cutoff
  // using current price
  let buy_cutoff_price_before = get_progression_defaults();
  let buy_cutoff_qty_before = get_progression_defaults();

  // using last price data
  let buy_cutoff_price_after = ownData.buy_cutoff_price || get_progression_defaults();
  let buy_cutoff_qty_after = get_progression_defaults();

  // using current price
  let sell_cutoff_price_before = get_progression_defaults();
  let sell_cutoff_qty_before = get_progression_defaults();

  // using last price data
  let sell_cutoff_price_after = ownData.sell_cutoff_price || get_progression_defaults();
  let sell_cutoff_qty_after = get_progression_defaults();

  // get snatchIt
  if (typeof dataRaw.sells[1] !== 'undefined') {
    tmp.snatchIt = {
      buy: dataRaw.sells[0].unit_price,
      sell: dataRaw.sells[1].unit_price,
      quantity: dataRaw.sells[0].quantity
    }
  }

  let snipeItTmpArray: All_Info_Snipe_It_Tmp[] = []
  let snipeItQuantityTemp = 0
  let snipeItValueTemp = 0

  for (let k = 0; k < dataRaw.sells.length; k++) {
    let progression:number = k / dataRaw.sells.length
    for (let l = 0; l < progressionValues.length; l++) {
      // Before =  the current one
      // after is based on last runs prices


      // use progression to get the before values
      if (progression <= progressionValues[l]) {
        // price cutoff for teh next run
        sell_cutoff_price_before[progressionValues[l]] = dataRaw.sells[k].unit_price

        // uses current price to do the current breakdown
        sell_cutoff_qty_before[progressionValues[l]] += dataRaw.sells[k].quantity
      }

      ///*
      if (dataRaw.sells[k].unit_price <= sell_cutoff_price_after[progressionValues[l]]) {
        // uses previous price to do the current breakdown
        sell_cutoff_qty_after[progressionValues[l]] += dataRaw.sells[k].quantity
      }

      //*/
    }

    tmp.sell_quantity += dataRaw.sells[k].quantity
    tmp.sell_listings += dataRaw.sells[k].listings

    snipeItQuantityTemp += dataRaw.sells[k].quantity
    snipeItValueTemp += dataRaw.sells[k].quantity * dataRaw.sells[k].unit_price


    let temp_profit = ((dataRaw.sells[k].unit_price * 0.85) * snipeItQuantityTemp) - snipeItValueTemp
    let temp_sorting = temp_profit / Math.pow(snipeItQuantityTemp, 2)

    let temp: All_Info_Snipe_It_Tmp = {
      avgBuyPrice: snipeItValueTemp / snipeItQuantityTemp,
      buyMax: dataRaw.sells[k].unit_price,
      buyMin: dataRaw.sells[0].unit_price,
      quantity: snipeItQuantityTemp,
      sellPrice: dataRaw.sells[k].unit_price,
      profit: temp_sorting,
      sorting: temp_sorting
    }
    snipeItTmpArray.push(temp)
  }

  snipeItTmpArray = snipeItTmpArray.sort((a, b) => sorter(a['sorting'], b['sorting'], false))

  if (snipeItTmpArray.length > 0) {
    delete snipeItTmpArray[0].profit
    delete snipeItTmpArray[0].sorting
    tmp.snipeIt = snipeItTmpArray[0]
  }

  for (let k = 0; k < dataRaw.buys.length; k++) {
    tmp.buy_quantity += dataRaw.buys[k].quantity
    tmp.buy_listings += dataRaw.buys[k].listings

    let progression = k / dataRaw.sells.length
    for (let l = 0; l < progressionValues.length; l++) {
      // Before =  the current one
      // after is based on last runs prices

      if (progression <= progressionValues[l]) {
        // price cutoff for teh next run
        buy_cutoff_price_before[progressionValues[l]] = dataRaw.buys[k].unit_price

        // uses current price to do the current breakdown
        buy_cutoff_qty_before[progressionValues[l]] += dataRaw.buys[k].quantity
      }

      if (dataRaw.buys[k].unit_price >= buy_cutoff_price_after[progressionValues[l]]) {
        // uses previous price to do the current breakdown
        buy_cutoff_qty_after[progressionValues[l]] += dataRaw.buys[k].quantity
      }
    }
  }

  if (dataRaw.sells.length > 0) {
    tmp.sell_price = dataRaw.sells[0].unit_price
  }
  if (dataRaw.buys.length > 0) {
    tmp.buy_price = dataRaw.buys[0].unit_price
  }


  let marketData: Market_Minute = {
    id: `${tmp.lastUpdate}_${tmp.id}`,
    iso:  tmp.lastUpdate,
    itemID: tmp.id,
    lastUpdate: tmp.lastUpdate,
    date: tmp.lastUpdate,
    deleteDate: new Date(dateAgo("hours", -3)).getTime(),


    buy_cutoff_price_after: buy_cutoff_price_after,
    buy_cutoff_price_before: buy_cutoff_price_before,
    buy_cutoff_qty_after: buy_cutoff_qty_after,
    buy_cutoff_qty_before: buy_cutoff_qty_before,
    buy_listings: 0,
    buy_price: 0,
    buy_quantity: 0,


    sell_cutoff_price_after: sell_cutoff_price_after,
    sell_cutoff_price_before: sell_cutoff_price_before,
    sell_cutoff_qty_after: sell_cutoff_qty_after,
    sell_cutoff_qty_before: sell_cutoff_qty_before,
    sell_listings: 0,
    sell_price: 0,
    sell_quantity: 0
  }

  if(tmp.buy_quantity > 0){
    marketData.buy_quantity = tmp.buy_quantity
    marketData.buy_price = tmp.buy_price
    marketData.buy_listings = tmp.buy_listings
  }

  if(tmp.sell_quantity > 0){
    marketData.sell_quantity = tmp.sell_quantity
    marketData.sell_price = tmp.sell_price
    marketData.sell_listings = tmp.sell_listings
  }


  tmp.buy_cutoff_price = buy_cutoff_price_before
  tmp.sell_cutoff_price = sell_cutoff_price_before

  tmp.profit = ((tmp.sell_price * 0.85) - tmp.buy_price) || 0

  let roi = ((tmp.profit/tmp.buy_price)*10000).toFixed(3)
  tmp.roi = parseInt(roi,10)/10000 || 0
  tmp.profit = Math.round(tmp.profit) || 0

  // if there are no listings then dont return these values to all data
  if(dataRaw.sells.length === 0){
    delete tmp.sell_quantity
    delete tmp.sell_price
    delete tmp.roi
    delete tmp.profit
  }
  if(dataRaw.buys.length === 0){
    delete tmp.buy_quantity
    delete tmp.buy_price
    delete tmp.roi
    delete tmp.profit
  }

  return {
    data_AllData: tmp,
    data_market: marketData,
  }
}

function getIDStrings (ids: number[], length: number = 200): string[]{
  let idsStr: string[] = []
  let tmp: number[] = []
  for (let i = 0; i < ids.length; i++) {
    if (tmp.length < length) {
      tmp.push(ids[i])
    } else {
      idsStr.push(tmp.join(','))
      tmp = [ids[i]]
    }
  }
  if (tmp.length > 0) {
    idsStr.push(tmp.join(','))
  }
  return idsStr
}