import {Collection,Index} from "./db";


interface Achievements_Tiers {
    count: number;
    points: number;
}
interface Achievements_Rewards {
    type: number;
    id?: number;
    count?: number;
    region?: string;
}
interface Achievements_Bits {
    type: string;
    id?: number;
    text?: string;
}

@Collection()
@Index(["id"])
export class Achievements {
    id: number;
    icon?: string;
    name: string;
    description: string;
    requirement: string;
    locked_text: string;
    type: string;
    flags: string[];
    tiers: Achievements_Tiers[];
    prerequisites?: number[];
    rewards?: Achievements_Rewards[];
    bits?: Achievements_Bits[];
    point_cap?: number;
    firstAdded?: string;
    lastUpdate: string;
}


export class Cutoff {
    "0": number;
    "0.0005": number;
    "0.001": number;
    "0.005": number;
    "0.01": number;
    "0.015": number;
    "0.02": number;
    "0.05": number;
    "0.1": number;
    "0.2": number;
    "0.25": number;
    "0.3": number;
    "0.4": number;
    "0.5": number;
}


export class All_Info_Snatch_It {
    buy: number;
    sell: number;
    quantity: number;
}
export class All_Info_Snipe_It {
    buyMin: number;
    buyMax: number;
    quantity: number;
    avgBuyPrice: number;
    sellPrice: number;
}

@Collection()
@Index(["id", "name", "firstAdded", "marketable"])
export class All_Info {
    // always available
    id: number;
    AccountBindOnUse?: boolean;
    AccountBound?: boolean;
    DeleteWarning?: boolean;
    NoSalvage?: boolean;
    NoSell?: boolean;
    SoulBindObUse?: boolean;
    NoMysticForge?: boolean;
    SoulBindOnAcquire?: boolean;
    NoUnderwater?: boolean;

    charm?: string;
    upgrade1?: number;
    upgrade2?: number;
    binding?: string;

    chat_link?: string;
    description?: string;
    
    img?: string;
    img_official?: string;
    
    level?: number;
    name?: string;
    rarity?: string;
    type?: string;
    vendor_value?: number;
    weaponType?: string;
    default_skin?: number;
    statID?: number;
    statName?: string;
    weight?: string;

    firstAdded?: string;
    lastUpdate: string;

    marketable?: boolean;
    craftable?: boolean;

    ////////////////////////////
    ////////////////////////////
    // market data stuff
    ////////////////////////////
    ////////////////////////////

    sell_listings?: number;
    sell_price?: number;
    sell_quantity?: number;

    profit?: number;
    roi?: number;

    snatchIt: All_Info_Snatch_It;
    snipeIt?: All_Info_Snipe_It;

    buy_listings?: number;
    buy_price?: number;
    buy_quantity?: number;

    buy_cutoff_price?: Cutoff;
    sell_cutoff_price?: Cutoff;

    
    "12m_buy_delisted"?: number;
    "12m_buy_listed"?: number;
    "12m_buy_sold"?: number;
    "12m_buy_value"?: number;
    "12m_buy_price_avg"?: number;
    "12m_buy_price_max"?: number;
    "12m_buy_price_min"?: number;
    "12m_buy_quantity_avg"?: number;
    "12m_buy_quantity_max"?: number;
    "12m_buy_quantity_min"?: number;

    "12m_sell_delisted"?: number;
    "12m_sell_listed"?: number;
    "12m_sell_sold"?: number;
    "12m_sell_value"?: number;
    "12m_sell_delisted_value"?: number;
    "12m_sell_price_avg"?: number;
    "12m_sell_price_max"?: number;
    "12m_sell_price_min"?: number;
    "12m_sell_quantity_avg"?: number;
    "12m_sell_quantity_max"?: number;
    "12m_sell_quantity_min"?: number;

    "3m_buy_delisted"?: number;
    "3m_buy_listed"?: number;
    "3m_buy_sold"?: number;
    "3m_buy_value"?: number;
    "3m_buy_price_avg"?: number;
    "3m_buy_price_max"?: number;
    "3m_buy_price_min"?: number;
    "3m_buy_quantity_avg"?: number;
    "3m_buy_quantity_max"?: number;
    "3m_buy_quantity_min"?: number;

    "3m_sell_delisted"?: number;
    "3m_sell_listed"?: number;
    "3m_sell_sold"?: number;
    "3m_sell_value"?: number;
    "3m_sell_delisted_value"?: number;
    "3m_sell_price_avg"?: number;
    "3m_sell_price_max"?: number;
    "3m_sell_price_min"?: number;
    "3m_sell_quantity_avg"?: number;
    "3m_sell_quantity_max"?: number;
    "3m_sell_quantity_min"?: number;

    "1m_buy_delisted"?: number;
    "1m_buy_listed"?: number;
    "1m_buy_sold"?: number;
    "1m_buy_value"?: number;
    "1m_buy_price_avg"?: number;
    "1m_buy_price_max"?: number;
    "1m_buy_price_min"?: number;
    "1m_buy_quantity_avg"?: number;
    "1m_buy_quantity_max"?: number;
    "1m_buy_quantity_min"?: number;

    "1m_sell_delisted"?: number;
    "1m_sell_listed"?: number;
    "1m_sell_sold"?: number;
    "1m_sell_value"?: number;
    "1m_sell_delisted_value"?: number;
    "1m_sell_price_avg"?: number;
    "1m_sell_price_max"?: number;
    "1m_sell_price_min"?: number;
    "1m_sell_quantity_avg"?: number;
    "1m_sell_quantity_max"?: number;
    "1m_sell_quantity_min"?: number;

    "7d_buy_delisted"?: number;
    "7d_buy_listed"?: number;
    "7d_buy_sold"?: number;
    "7d_buy_value"?: number;
    "7d_buy_price_avg"?: number;
    "7d_buy_price_max"?: number;
    "7d_buy_price_min"?: number;
    "7d_buy_quantity_avg"?: number;
    "7d_buy_quantity_max"?: number;
    "7d_buy_quantity_min"?: number;

    "7d_sell_delisted"?: number;
    "7d_sell_listed"?: number;
    "7d_sell_sold"?: number;
    "7d_sell_value"?: number;
    "7d_sell_delisted_value"?: number;
    "7d_sell_price_avg"?: number;
    "7d_sell_price_max"?: number;
    "7d_sell_price_min"?: number;
    "7d_sell_quantity_avg"?: number;
    "7d_sell_quantity_max"?: number;
    "7d_sell_quantity_min"?: number;

    "2d_buy_delisted"?: number;
    "2d_buy_listed"?: number;
    "2d_buy_sold"?: number;
    "2d_buy_value"?: number;
    "2d_buy_price_avg"?: number;
    "2d_buy_price_max"?: number;
    "2d_buy_price_min"?: number;
    "2d_buy_quantity_avg"?: number;
    "2d_buy_quantity_max"?: number;
    "2d_buy_quantity_min"?: number;

    "2d_sell_delisted"?: number;
    "2d_sell_listed"?: number;
    "2d_sell_sold"?: number;
    "2d_sell_value"?: number;
    "2d_sell_delisted_value"?: number;
    "2d_sell_price_avg"?: number;
    "2d_sell_price_max"?: number;
    "2d_sell_price_min"?: number;
    "2d_sell_quantity_avg"?: number;
    "2d_sell_quantity_max"?: number;
    "2d_sell_quantity_min"?: number;

    "1d_buy_delisted"?: number;
    "1d_buy_listed"?: number;
    "1d_buy_sold"?: number;
    "1d_buy_value"?: number;
    "1d_buy_price_avg"?: number;
    "1d_buy_price_max"?: number;
    "1d_buy_price_min"?: number;
    "1d_buy_quantity_avg"?: number;
    "1d_buy_quantity_max"?: number;
    "1d_buy_quantity_min"?: number;

    "1d_sell_delisted"?: number;
    "1d_sell_listed"?: number;
    "1d_sell_sold"?: number;
    "1d_sell_value"?: number;
    "1d_sell_delisted_value"?: number;
    "1d_sell_price_avg"?: number;
    "1d_sell_price_max"?: number;
    "1d_sell_price_min"?: number;
    "1d_sell_quantity_avg"?: number;
    "1d_sell_quantity_max"?: number;
    "1d_sell_quantity_min"?: number;
}


@Collection()
@Index(["id"])
export class Build {
    id: number;
    iso: string;
    epoch: number;
}

@Collection()
@Index(["iso"])
export class Gem_Price_Gold {
    iso: string;
    price: number;
}
@Collection()
@Index(["iso"])
export class Gem_Price_Gem {
    iso: string;
    price: number;
}

@Collection()
@Index(["id"])
export class Currencies {
    id: number;
    description: string;
    firstAdded?: string;
    icon: string;
    lastUpdate: string;
    name: string;
    order: number;
}

export class gem_store_catalog_categoryLifespans_items {
    start: string;
    end: string;
}

export class gem_store_catalog_categoryLifespans {
    [propName: string]: gem_store_catalog_categoryLifespans_items[]
}

export class gem_store_catalog_categoryLifespans_old {
    category: string;
    start: string;
    end: string;
}

export class gem_store_catalog_contents {
    disablePreview: boolean;
    guid: string;
    quantity: number;
}
export class gem_store_catalog_passwords {
    [propName: string]: string
}

@Collection()
@Index(["uuid"])
export class Gem_Store_Catalog {
    uuid: string;

    blurb: string
    categories?: string[]
    categoriesOld?: string[];
    categoryLifespans?: gem_store_catalog_categoryLifespans;
    categoryLifespansOld?: gem_store_catalog_categoryLifespans_old[];
    contentGuid: string;
    contents?: gem_store_catalog_contents[];
    dataId?: number;
    description: string;
    excludedFeatures: number[];
    firstAdded?: string;
    gemPrice?: number;
    gemStoreBannerItem?: string;
    giftable?: boolean;
    hide?: boolean;
    imageHash: string;
    itemGuid?: string;
    latestEndDate?: string;
    latestStartDate?: string;
    name: string;
    passwords?: gem_store_catalog_passwords;
    randomchance?: boolean
    requiredFeatures: number[];
    requirementMissing: string;
    returning?: boolean;
    sku?: number;
    type: string;
}

export class gem_store_categories_childLifespans_element{
    start: string;
    end: string;
}
export class gem_store_categories_childLifespans {
    [propName: string]: gem_store_categories_childLifespans_element[] | undefined;
}

export class gem_store_categories_featuredItemLifespan {
    [propName: string]: gem_store_categories_childLifespans_element[] | undefined;
}

@Collection()
@Index(["uuid"])
export class Gem_Store_Categories {
    uuid: string;

    addendum: string;
    childCategories: string[];
    childLifespans?: gem_store_categories_childLifespans
    description: string;
    displayname: string;
    featuredItemLifespan?: gem_store_categories_featuredItemLifespan
    featuredItems: string[];
    firstAdded?: string;
    name: string;
    parentCategories: string[];
}

@Collection()
@Index(["build"])
export class Gem_Store_Index {
    build: number;
    items: string[];
}

export class item_stats_attributes {
    attribute: string;
    multiplier: number;
    value: number;
}

@Collection()
@Index(["id"])
export class Item_Stats {
    id: number;

    attributes: item_stats_attributes[];
    firstAdded?: string;
    lastUpdate: string;
    name: string;
}

export class item_upgrades {
    item_id: number;
    upgrade: string;
}

@Collection()
@Index(["id"])
export class Item {
    id: number;

    chat_link: string;
    default_skin?: number;
    description?: string;
    description_de?: string;
    description_es?: string;
    description_fr?: string;

    // not going to map this out entirely
    details?: any;

    firstAdded?: string;
    lastUpdate?: string;

    flags: string[];
    game_types: string[];
    icon?: string;
    level: number;
    name: string;
    name_de?: string;
    name_es?: string;
    name_fr?: string;

    rarity: string;
    restrictions: string[];
    type: string;
    upgrades_from: item_upgrades[];
    upgrades_into: item_upgrades[];
    vendor_value: number;
}

// leave marketdata stuff till the end

// marketDataArchive


// this is what it looks like just after obtaining the data
@Collection()
@Index(["id", "date", "deleteDate", "itemID"])
export class Market_Minute {
    id: string;
    date: string;
    deleteDate: number;
    itemID: number;
    
    iso: string;
    lastUpdate: string;

    buy_listings: number;
    buy_price: number;
    buy_quantity: number;

    buy_cutoff_price_before: Cutoff;
    buy_cutoff_price_after: Cutoff;
    buy_cutoff_qty_before: Cutoff;
    buy_cutoff_qty_after: Cutoff;

    sell_listings: number;
    sell_price: number;
    sell_quantity: number;

    sell_cutoff_price_before: Cutoff;
    sell_cutoff_price_after: Cutoff;
    sell_cutoff_qty_before: Cutoff;
    sell_cutoff_qty_after: Cutoff;
}

// almost final step, fairly simple one in comparion to the enxt one
export abstract class Market_Base {
    id: string;
    date: string;
    itemID: number;
    count: number;

    buy_count: number;
    buy_delisted: number;
    buy_listed: number;
    buy_price_avg: number;
    buy_price_max: number;
    buy_price_min: number;
    buy_price_stdev: number;
    buy_quantity_avg: number;
    buy_quantity_max: number;
    buy_quantity_min: number;
    buy_quantity_stdev: number;
    buy_sold: number;
    buy_value: number;

    sell_count: number;
    sell_delisted: number;
    sell_delisted_value: number;
    sell_listed: number;
    sell_price_avg: number;
    sell_price_max: number;
    sell_price_min: number;
    sell_price_stdev: number;
    sell_quantity_avg: number;
    sell_quantity_max: number;
    sell_quantity_min: number;
    sell_quantity_stdev: number;
    sell_sold: number;
    sell_value: number;
}

// this is exactly the same as the abvse
@Collection()
@Index(["id", "date", "deleteDate", "itemID"])
export class Market_Hour extends Market_Base {
    // this is only on the hour one
    deleteDate: number;
}

@Collection()
@Index(["id", "date", "itemID"])
export class Market_Day extends Market_Base {

}
// marketDataArchiveTemp

// this is the totals for the entire market
@Collection()
@Index(["date"])
export class Market_Total {
    date: string;

    count: number;

    buy_delisted: number;
    buy_listed: number;
    buy_sold: number;
    buy_value: number;

    sell_delisted: number;
    sell_delisted_value: number;
    sell_listed: number;
    sell_sold: number;
    sell_value: number;
}

@Collection()
@Index(["row"])
export class Festivals {
    row: number;

    year: number;
    festival: string;
    start: string;
    end: string;
    type: string;
    id: number;
    quantity: number;
}

@Collection()
@Index(["row"])
export class Raid_Marketable {
    row: number;

    cost: number;
    currency: string;
    gold: number;
    id: number;
}


export class Recipes_Guild_Ingredients {
    count: number;
    upgrade_id: number;
}
export class Recipes_Ingredients {
    count: number;
    item_id: number;
}

@Collection()
@Index(["id"])
export class Recipes {
    id: number;

    achievement_id?: number;
    chat_link?: string;
    disciplines: string[];
    firstAdded?: string;
    lastUpdate?: string;
    flags?: string[];
    guild_ingredients?: Recipes_Guild_Ingredients[];
    ingredients: Recipes_Ingredients[];
    min_rating?: number;
    name?: string;
    output_item_count: number;
    output_item_id: number;
    output_upgrade_id?: number;
    time_to_craft_ms?: number;
    type: string;
}

// rust style
class skins_details_dye_slots_default {
    color_id: number;
    material: string;
}
class skins_details_dye_slots_overrides {
    [propName: string]:skins_details_dye_slots_default[]
}
class skins_details_dye_slots {
    default: skins_details_dye_slots_default[];
    overrides: skins_details_dye_slots_overrides;
}
class skins_details {
    damage_type?: string;
    dye_slots?: skins_details_dye_slots;
}

@Collection()
@Index(["id"])
export class Skins {
    id: number;

    description?: string;
    details?: skins_details;
    flags: string[];
    icon?: string;
    name: string;
    rarity: string;
    restrictions: string[];
    type: string;
    firstAdded?: string;
    lastUpdate?: string;
}

// this is an array of all the entities
export const db_entities: (new () => any)[] = [
    Achievements,
    All_Info,
    Build,
    Currencies,
    Gem_Store_Catalog,
    Gem_Store_Categories,
    Gem_Store_Index,

    Gem_Price_Gold,
    Gem_Price_Gem,

    Item_Stats,
    Item,

    Market_Total,
    Festivals,
    Raid_Marketable,
    Recipes,
    Skins,


    Market_Minute,
    Market_Hour,
    Market_Day
];