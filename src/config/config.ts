import dotenv from 'dotenv'
import packageData from '../../package.json'

dotenv.config();

const dbPort = process.env.DB_PORT || 27017;
const dbLocation = process.env.DB_LOCATION;
const dbUser = process.env.DB_USER;
const dbPass = process.env.DB_PASS;
let dbURL = 'mongodb://' + dbUser + ':' + dbPass + '@' + dbLocation + ':' + dbPort + '/admin';
if (dbUser === '' || dbPass === '') {
  dbURL = 'mongodb://' + dbLocation + ':' + dbPort
}

if (dbLocation.indexOf(".") !== -1) {
  dbURL = 'mongodb+srv://' + dbUser + ':' + dbPass + '@' + dbLocation + '/admin?ssl=false'
}

let cutoffPercentagesRaw = process.env.CUT_OFF_PERCENTAGE || "0,0.0005,0.001,0.005,0.01,0.015,0.02,0.05,0.1,0.2,0.25,0.3,0.4,0.5"

const config = {
  name: packageData.name,
  version: packageData.version,
  env: process.env.NODE_ENV || process.env.ENVIROMENT,
  db: {
    uri: dbURL,
    database: process.env.DB
  },
  port: process.env.PORT || 97,
  proxy: process.env.PROXY || "",
  cutoffPercentages: cutoffPercentagesRaw.split(","),
  googleSheets: process.env.GOOGLE_SHEETS || "",
}

export default config
export const { name, version, env, db, port, proxy, cutoffPercentages, googleSheets} = config